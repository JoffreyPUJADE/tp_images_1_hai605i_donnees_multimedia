# TP_Images_1_HAI605I_Données_Multimédia

Programmation du premier TP de l'UE HAI605I - Données Multimédia.

# Sommaire

* [Liste des exécutables pouvant être produits](#liste-des-exécutables-pouvant-être-produits)
* [Génération des graphiques avec GNUPLOT](#génération-des-graphiques-avec-gnuplot)
	* [Depuis les données générées avec histo](#depuis-les-données-générées-avec-histo)
	* [Depuis les données générées avec profil](#depuis-les-données-générées-avec-profil)
	* [Depuis les données générées avec histoCouleurs](#depuis-les-données-générées-avec-histocouleurs)
* [Sources](#sources)
	* [Sources ayant aidé à l'utilisation de GNUPLOT](#sources-ayant-aidé-à-lutilisation-de-gnuplot)

# Liste des exécutables pouvant être produits

Il est possible, avec les différentes sources de ce TP, de produire plusieurs exécutables.

Ceux-ci sont :

* Depuis le fichier "test_grey.cpp" : Seuillage d'une image pgm en niveau de gris, avec un seul seuil
* Depuis le fichier "grey2Seuils.cpp" : Seuillage d'une image pgm en niveau de gris, avec deux seuils
* Depuis le fichier "grey3Seuils.cpp" : Seuillage d'une image pgm en niveau de gris, avec trois seuils
* Depuis le fichier "histo.cpp" : Création d'un histogramme d'une image pgm
* Depuis le fichier "profil.cpp" : Création d'un histogramme d'une colonne ou d'une ligne d'une image pgm
* Depuis le fichier "couleursSeuil.cpp" : Seuillage d'une image couleur (ppm)
* Depuis le fichier "histoCouleurs.cpp" : Histogrammes des 3 composantes d'une image couleur (ppm)

# Génération des graphiques avec GNUPLOT

## Depuis les données générées avec histo

Pour générer un graphique avec GNUPLOT, via les données générées avec le logiciel produit
via le fichier "histo.cpp", il suffit de saisir la commande suivante :

`plot [t=0:256] "vos_donnees.bat" with lines`

## Depuis les données générées avec profil

Pour générer un graphique avec GNUPLOT, via les données générées avec le logiciel produit
via le fichier "profil.cpp", il suffit de saisir la commande suivante :

`plot [t=0:256] "vos_donnees.bat" with lines`

## Depuis les données générées avec histoCouleurs

Pour générer un graphique avec GNUPLOT, via les données générées avec le logiciel produit
via le fichier "histoCouleurs.cpp", il suffit de saisir la commande suivante :

`p [t=0:256] for [col = 1:3] "vos_donnees.bat" using 1:col w lp`

# Sources

* [HAI605I : Données multimédia](http://www.lirmm.fr/~wpuech/enseignement/donnees_multimedia/)
* [TP1 Prise en main d'une librairie de traitement d'images](http://www.lirmm.fr/~wpuech/enseignement/donnees_multimedia/TP1/TP1_Image.pdf)

## Sources ayant aidé à l'utilisation de GNUPLOT

* [How to Print Plots from Gnuplot and line types](https://web.math.utk.edu//~vasili/refs/How-to/gnuplot.print.html#:~:text=gnuplot%3E%20set%20term%20png%20(will,want%20to%20do%20with%20it.))
* [Gnuplot](https://doc.ubuntu-fr.org/gnuplot)
* [Plot a single data file](https://riptutorial.com/gnuplot/example/12382/plot-a-single-data-file)
