#ifndef TRANSFORMATIONS_HPP
#define TRANSFORMATIONS_HPP

#include "image_ppm.h"
#include <algorithm>
#include <vector>

/*!
 * \brief Seuille une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S (int) : Seuil de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris.
*/
static inline void seuillageGris(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int S)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(ImgIn[i*nW+j] < S)
			{
				ImgOut[i*nW+j] = 0;
				
				if(ImgIn[i*nW+j - 1] < S)
					ImgOut[i*nW+j - 1] = 0;
				if(ImgIn[i*nW+j + 1] < S)
					ImgOut[i*nW+j + 1] = 0;
			}
			else
			{
				ImgOut[i*nW+j] = 255;
			}
		}
	}
}

/*!
 * \brief Seuille une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S1 (int) : Premier seuil de l'image.
 * \param [in] S2 (int) : Second seuil de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris avec deux seuils.
*/
static inline void seuillageGris2Seuils(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int S1, int S2)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(ImgIn[i*nW+j] < S1)
				ImgOut[i*nW+j] = 0;
			else if(ImgIn[i*nW+j] < S2)
				ImgOut[i*nW+j]=128;
			else
				ImgOut[i*nW+j] = 255;
		}
	}
}

/*!
 * \brief Seuille une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S1 (int) : Premier seuil de l'image.
 * \param [in] S2 (int) : Second seuil de l'image.
 * \param [in] S3 (int) : Troisième seuil de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris avec trois seuils.
*/
static inline void seuillageGris3Seuils(OCTET* ImgIn, OCTET* ImgOut, int nH, int nW, int S1, int S2, int S3)
{
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(ImgIn[i*nW+j] < S1)
				ImgOut[i*nW+j]=0;
			else if(ImgIn[i*nW+j] < S2)
				ImgOut[i*nW+j]=85;
			else if(ImgIn[i*nW+j] < S3)
				ImgOut[i*nW+j]=170;
			else
				ImgOut[i*nW+j]=255;

		}
	}
}

/*!
 * \brief Crée un histogramme d'une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, dont on doit obtenir l'histogramme.
 * \param [in] nTaille (int) : Taille de l'image.
 * \details Cette fonction permet de créer les données d'un histogramme d'une image en niveau de gris.
 * \return Données de l'histogramme (std::vector<int>).
 */
static inline std::vector<int> histogrammeImage(OCTET* ImgIn, int nTaille)
{
	std::vector<int> nbOccurences;
	
	nbOccurences.resize(256);
	
	std::fill(nbOccurences.begin(), nbOccurences.end(), 0);
	
	for(int i=0;i<nTaille;++i)
	{
		++nbOccurences[((int)ImgIn[i])];
	}
	
	return nbOccurences;
}

/*!
 * \brief Crée un profil pour une image en niveau de gris.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à dont on doit créer un profil.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \details Cette fonction permet de créer le profil d'une ligne ou d'une colonne d'une image en niveau de gris.
 * \return Données du profil (std::vector<unsigned int>).
 */
static inline std::vector<unsigned int> profilImage(OCTET* ImgIn, int nH, int nW, char typeChoix, int indice)
{
	std::vector<unsigned int> tabProfil;
	
	for(int i=0;i<nH;++i)
	{
		for(int j=0;j<nW;++j)
		{
			if(typeChoix == 'c')
			{
				if(j == indice)
					tabProfil.push_back(static_cast<unsigned int>(ImgIn[i]));
			}
			else if(typeChoix == 'l')
			{
				if(i == indice)
					tabProfil.push_back(static_cast<unsigned int>(ImgIn[j]));
			}
		}
	}
	
	return tabProfil;
}

/*!
 * \brief Seuille une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, à seuiller.
 * \param [inout] ImgOut (OCTET*) : Image de sortie, contenant l'image seuillée.
 * \param [in] nTaille3 (int) : Taille de l'image en couleur.
 * \param [in] nH (int) : Hauteur de l'image.
 * \param [in] nW (int) : Largeur de l'image.
 * \param [in] S_R (int) : Seuil pour la couleur rouge de l'image.
 * \param [in] S_G (int) : Seuil pour la couleur verte de l'image.
 * \param [in] S_B (int) : Seuil pour la couleur bleue de l'image.
 * \details Cette fonction permet de seuiller une image en niveau de gris.
*/
static inline void seuillageCouleur(OCTET* ImgIn, OCTET* ImgOut, int nTaille3, int S_R, int S_G, int S_B)
{
	for (int i=0;i<nTaille3;i+=3)
	{
		int nR = ImgIn[i];
		int nG = ImgIn[i+1];
		int nB = ImgIn[i+2];
		
		if(nR < S_R)
			ImgOut[i]=0;
		else
			ImgOut[i]=255;
		
		if(nG < S_G)
			ImgOut[i+1]=0;
		else
			ImgOut[i+1]=255;
		
		if(nB < S_B)
			ImgOut[i+2]=0;
		else
			ImgOut[i+2]=255;
	}
}

/*!
 * \brief Crée un histogramme d'une image en couleur.
 * \param [inout] ImgIn (OCTET*) : Image d'entrée, dont on doit obtenir l'histogramme.
 * \param [in] nTaille3 (int) : Taille de l'image en couleur.
 * \details Cette fonction permet de créer les données d'un histogramme d'une image en couleur.
 * \return Données de l'histogramme (std::vector<std::vector<int> >).
 */
static inline std::vector<std::vector<int> > histogrammeCouleurImage(OCTET* ImgIn, int nTaille3)
{
	std::vector<std::vector<int> > nbOccurences;
	
	nbOccurences.push_back(std::vector<int>());
	nbOccurences.push_back(std::vector<int>());
	nbOccurences.push_back(std::vector<int>());
	
	int nbOccurencesRouge[256] = { 0 };
	int nbOccurencesVert[256] = { 0 };
	int nbOccurencesBleu[256] = { 0 };
	
	for(int i=0;i<nTaille3;i+=3)
	{
		++nbOccurencesRouge[((int)ImgIn[i])];
		++nbOccurencesVert[((int)ImgIn[i+1])];
		++nbOccurencesBleu[((int)ImgIn[i+2])];
	}
	
	for(size_t i=0;i<256;++i)
	{
		nbOccurences[0].push_back(nbOccurencesRouge[i]);
		nbOccurences[1].push_back(nbOccurencesVert[i]);
		nbOccurences[2].push_back(nbOccurencesBleu[i]);
	}
	
	return nbOccurences;
}

#endif // TRANSFORMATIONS_HPP