#include <stdio.h>
#include "Transformations.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>

template<typename T>
std::string type2String(T valeur)
{
	std::ostringstream oss;
	
	oss << valeur;
	
	return oss.str();
}

std::string nomFichierSansExtension(std::string nomFichier)
{
	std::string res = "";
	size_t index = 0;
	
	while(nomFichier[index] != '.')
	{
		res += nomFichier[index];
		
		++index;
	}
	
	return res;
}

int main(int argc, char* argv[])
{
	char cNomImgLue[250];
	int nH, nW, nTaille;
	
	if(argc != 2) 
	{
		printf("Usage: ImageIn.ppm \n"); 
		exit(1);
	}
	
	sscanf(argv[1],"%s",cNomImgLue);
	
	OCTET *ImgIn;
	
	lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
	nTaille = nH * nW;
	
	int nTaille3 = nTaille * 3;
	allocation_tableau(ImgIn, OCTET, nTaille3);
	lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
	
	std::vector<std::vector<int> > nbOccurences = histogrammeCouleurImage(ImgIn, nTaille3);
	
	free(ImgIn);
	
	std::string nomFichierResultats = (nomFichierSansExtension(type2String<char*>(cNomImgLue)) + ".dat");
	
	std::ofstream fluxEcriture;
	
	fluxEcriture.open(nomFichierResultats);
	
	if(!fluxEcriture.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : Impossible d'ecrire dans le fichier \"" << nomFichierResultats << "\".";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	for(size_t i=0;i<256;++i)
	{
		std::cout << i << " " << nbOccurences[0][i] << " " << nbOccurences[1][i] << " " << nbOccurences[2][i] << ((i < (256 - 1)) ? "\n" : "");
		fluxEcriture << i << " " << nbOccurences[0][i] << " " << nbOccurences[1][i] << " " << nbOccurences[2][i] << ((i < (256 - 1)) ? "\n" : "");
	}
	
	fluxEcriture.close();
	
	return 1;
}
